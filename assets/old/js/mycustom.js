
//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $(document).on('click', 'a.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
   
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

 