	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH . 'libraries/include.php';
	//header('Access-Control-Allow-Origin: *');
	class Welcome extends CI_Controller {

		/**
		* Index Page for this controller.
		*
		* Maps to the following URL
		* 		http://example.com/index.php/welcome
		*	- or -
		* 		http://example.com/index.php/welcome/index
		*	- or -
		* Since this controller is set as the default controller in
		* config/routes.php, it's displayed at http://example.com/
		*
		* So any other public methods not prefixed with an underscore will
		* map to /index.php/welcome/<method_name>
		* @see https://codeigniter.com/user_guide/general/urls.html
		*/
		public function __construct()  {  
			parent:: __construct();  
			$this->load->model('Data');  
			$this->load->helper('url');  
			$this->load->helper('form');  
			$this->load->library('user_agent');
			//$this->load->library('Googleplus');
		}  

		public function index()
		{
			//$this->load->view('welcome_message');
	$this->load->view('include/header_main.php');
			$this->load->view('pages/home');
		}


		public function getData($queryParameter=null,$orderBy=null)
		{	
			//print_r($this->agent);

			$this->load->helper("wordtonumber_helper");
			
			
			$orderBy= $this->input->get('orderBy', TRUE);
			/**
				for the order of showing result
			**/
			if($orderBy==''){
				$orderBy="desc";	
			}
			
			$queryParameter= $this->input->get('searchQuery', TRUE);
			$matchPercentage=preg_match("^\d+\%^",$queryParameter);
					
			/**

				for converting the word to number 
				convertToNumber($queryParameter); 
			**/
			
			
			$this->Data->userAgent($this->agent,$queryParameter); //for geetting the user information and query
			
			$data['queryParameter']=$this->input->get('searchQuery', TRUE);
			
			if($queryParameter){
			
			$page = ($this->input->get('page', TRUE)) ? $this->input->get('page', TRUE) : 0;

		if(!is_numeric($queryParameter)){ //string found
				//echo "not numeric"; exit;
				

				/**
					percentage search condition
				**/

				if($matchPercentage==1){
					echo "Yes i am in % search option";
					$matchPercentage=preg_match("^\d+\%^",$queryParameter,$matches);
					
					$newString=preg_replace("^\d+\%^",' ',$queryParameter);
					$data['wikipediaSearch']=$newString; //Wikipedia search variable
					//echo $newString; 

					$allData=$this->Data->percentageSearch($matches[0],$newString,$page,20);
					$totalRows=$this->Data->percentageSearchTotal($matches[0],$newString);
			
				}else{
					/** 
					Searching for the text 
				**/
					$query=explode(' ',$queryParameter);
					$checkNumberOccurence=count($query);
					$count=0;
					foreach($query as $num){
						if(is_numeric($num)){
						
							$count++;
						}
					}

					$data['wikipediaSearch']=$queryParameter;

					if($count==$checkNumberOccurence){
						//echo "number search witht multiple paremeters";
						$totalRows=$this->Data->numberSearcAll($query);
						//print_r($totalRows); exit;
						$allData=$this->Data->numberSearch($query,$page,20);
					}
					else{
					$allData=$this->Data->getData($queryParameter,$page,20,$orderBy);
					$totalRows=$this->Data->allData($queryParameter);
					}
			}
		}else { //number found
				
				/** 
					Searching for the number 
				**/
					//$queryParameter=strlen($queryParameter);
					if(strlen($queryParameter)>8){
						$checkLogic=substr($queryParameter,0,2); //for the first two digit is 99
						if($checkLogic==99)
						{
							$queryParameter=substr($queryParameter,0,6);	
						}
						$queryParameter=substr($queryParameter,0,7);
						$data['queryParameter'] =$queryParameter;
					}
				$totalRows=$this->Data->numberSearcAll($queryParameter);
				$allData=$this->Data->numberSearch($queryParameter,$page,20);
				
			}
			
			$data['allData']=$allData['results'];
			$data['totalResultRows']=$totalRows['totalRows'];
			
			// for pagination starts here

			$total_pages = ceil($totalRows['totalRows'] / 20);  
			$pagLink = "<ul class='pagination'>";  
			for ($i=1; $i<=$total_pages; $i++) {  
				if($this->input->get('page', TRUE)==$i){
				$pagLink .= "<li class='active'><a href='getData?searchQuery=".$queryParameter."&page=".$i."'>".$i."</a></li>"; 
				}else{
					$pagLink .= "<li><a href='getData?searchQuery=".$queryParameter."&page=".$i."'>".$i."</a></li>"; 
				}
			};  
			$pagLink . "</div>"; 
			$data['pagination']=$pagLink;
			if(!$data['wikipediaSearch']){
				$data['wikipediaSearch']="";
			}
			//paginbation part ends here 	
			/**
			setting view when there is not result found in database.
			**/
			if($totalRows['totalRows']>0){
				$data['noResultFound']=false;
			}else{
				$data['errorMessage']="No results found for $queryParameter";
				$data['noResultFound']=true;
			}
			$this->load->view('pages/summary.php',$data);
			//print_r($data);
		}else{  
			//number seach part starts here 
			//numberSearc($number);
			$data['noResultFound']=true;
			$data['errorMessage']="Search for the GST rates .$queryParameter";
			$this->load->view('pages/summary.php',$data);
			}
		}

		//for api 

		public function getDataApi($queryParameter=null)
		{	
			
			$queryParameter= $this->input->get('searchQuery', TRUE);	
			$data['queryParameter']=$this->input->get('searchQuery', TRUE);
			if($queryParameter){
			$this->load->model('Data');
			$page = ($this->input->get('page', TRUE)) ? $this->input->get('page', TRUE) : 0;
			$allData=$this->Data->getData($queryParameter,$page,20,'desc');
			$totalRows=$this->Data->allData($queryParameter);
			$data['allData']=$allData['results'];
			$data['totalResultRows']=$totalRows['totalRows'];
			// for pagination starts here
			$total_pages = ceil($totalRows['totalRows'] / 20);  
			$pagLink = "<ul class='pagination'>";  
			for ($i=1; $i<=$total_pages; $i++) {  
				if($this->input->get('page', TRUE)==$i){
				$pagLink .= "<li class='active'><a href='getData?searchQuery=".$queryParameter."&page=".$i."'>".$i."</a></li>"; 
				}else{
					$pagLink .= "<li><a href='getData?searchQuery=".$queryParameter."&page=".$i."'>".$i."</a></li>"; 
				}
			};  
			$pagLink . "</div>";  
			// /$data['pagination']=$pagLink;
			echo json_encode($data);		
			//paginbation part ends here 
			//$this->load->view('pages/searchResult.php',$data);
			//print_r($data);
		}else{
			echo "Enter query parameter";
			}
		}
			
		//dictionary function

		public function correctMe()
		{
			$this->load->view('pages/correctMe.php');
		}


		public function searchsuggetionnew(){  
			// process posted form data 
			/*$keyword = $this->input->post('term'); */
		$keyword = $this->input->post('keyword'); 
		
		$json = [];
			$query1 = $this->Data->searchsuggetion_second($keyword); //Search DB
			$query2 = $this->Data->searchsuggetion_third($keyword); //Search DB  
			$json = array_merge($query1,$query2);
		if(!empty($json)) {
	?>
	<ul id="country-list">
	<?php
	foreach($json as $searchdata) {
	?>

	<li onClick="selectCountry('<?php echo $searchdata["text"]; ?>');"><?php echo $searchdata["text"]; ?></li>
	<?php } ?>
	</ul>
	<?php } 
	
	
		} 

		// login starts here
		public function login(){ 
		$this->load->view('include/header_main.php'); 
			$this->load->view('pages/login.php');  
		} 
		// login ends here 

		//signup starts here

		public function signup(){  
	$this->load->view('include/header_main.php');
		$this->load->view('pages/signup.php');
	}
	//signup ends here

	public function logindetail()
	{
		$email=$this->input->post('email');
		$pwd = $this->input->post('pwd');
		$this->load->model('Data');
		$val=$this->Data->getuserdata($email,$pwd);
		
		if(count($val)>0){
			$userregid = $val[0]['id'];
					$this->session->set_userdata('registrationid', $userregid);
			echo "1"; //user logged in
		}
		else{
		echo "0";//user not registered
		}
	
	}
		public function logout()
	{
		$this->session->unset_userdata('registrationid');
		$this->load->view('pages/signup.php');
	}
	public function verify()
	{
		$this->load->model('Data');
		
		$email=$this->input->post('email');
		$availemail = $this->Data->availemail($email);
		if(count($availemail) <= 0){//if user not registered
		$ran_var = rand(100000,999999);
		$val=$this->Data->insert_random($ran_var);
		
		if(count($val)>0){ 
		// Email configuration 
			$newdata['value'] = array(
			'username' => $this->input->post('name'),
			'pri_email' => $this->input->post('email'),
			'rand_num' => $ran_var,
		);
		
	

			$from ='<rupali.jain@lyxellabs.com>';
			$to =  $this->input->post('email');
			$subject = 'Sign Up';
			$body =$this->load->view('signmail.php', $newdata, TRUE);
			
			$headers = array(
			'From' => $from,
			'To' => $this->input->post('email'),
			'Subject' => $subject,
			'MIME-Version' => '1.0',
			'Content-Type' => "text/html; charset=ISO-8859-1"
			);

		$smtp = Mail::factory('smtp', array(
			'host' => 'ssl://smtp.gmail.com',
			'port' => '465',
			'auth' => true,
			'username' => 'rupali.jain@lyxellabs.com',
			'password' => 'rupali-28basoya'
			));
		$mail = $smtp->send($to, $headers, $body);
		
		if (PEAR::isError($mail)) {
			echo "0"; //mail failed
			} else {
		$this->Data->logindata($val);
			echo "1"; //mail sent
		
			}
			}
			else{
			echo "4"; //something went wrong
			}
			
		}
		elseif(count($availemail) >= 0 && $availemail[0]["password"]==''){ //if user registered with social id
		$ran_var = rand(100000,999999);
		$this->Data->update_random($ran_var,$availemail[0]["id"]);
		
		// Email configuration 
			$newdata['value'] = array(
			'username' => $availemail[0]["Name"],
			'pri_email' => $availemail[0]["email"],
			'rand_num' => $ran_var,
		);
		
	

			$from ='<rupali.jain@lyxellabs.com>';
			$to =  $availemail[0]["email"];
			$subject = 'Sign Up';
			$body =$this->load->view('signmail.php', $newdata, TRUE);
			
			$headers = array(
			'From' => $from,
			'To' => $availemail[0]["email"],
			'Subject' => $subject,
			'MIME-Version' => '1.0',
			'Content-Type' => "text/html; charset=ISO-8859-1"
			);

		$smtp = Mail::factory('smtp', array(
			'host' => 'ssl://smtp.gmail.com',
			'port' => '465',
			'auth' => true,
			'username' => 'rupali.jain@lyxellabs.com',
			'password' => 'rupali-28basoya'
			));
		$mail = $smtp->send($to, $headers, $body);
		
		if (PEAR::isError($mail)) {
			echo "0"; //mail failed
			} else {
		$this->Data->updatelogindetail($availemail[0]["id"]);
			echo "1"; //mail sent
		
			}
			
		}
		else //if user registered
		{
		echo "3"; //email address already registered
		}
		
	}
	public function verifyotp()
	{
		
		$this->load->model('Data');
		$val=$this->Data->verifyuser();
		if(count($val)>0){
		$time1=$val[0]["updated_at"];
		$time2= date('Y-m-d H:i:s');
		$dteStart = new DateTime($time1);
			$dteEnd   = new DateTime($time2); 
		$dteDiff  = $dteStart->diff($dteEnd); 
		$mindelay= $dteDiff->format("%I"); 
		if($mindelay>=5){
			echo "0"; //OTP expired
		}
		else{
		$this->Data->updatestatus($val[0]["id"]);
		echo "1"; //user verified
		}
		}
		else{
		echo "2";//user do not exist
		}
	}
	
		public function resendotp()
	{
		$this->load->model('Data');
		if($this->input->post('email')){
		$ran_var = rand(100000,999999);
		$val=$this->Data->insert_otp($ran_var);
		
		if(count($val)>0){
		// Email configuration 
			$newdata['value'] = array(
			'username' => $this->input->post('name'),
			'pri_email' => $this->input->post('email'),
			'rand_num' => $ran_var,
		);
			$from ='<rupali.jain@lyxellabs.com>';
			$to =  $this->input->post('email');
			$subject = 'Sign Up';
			$body =$this->load->view('signmail.php', $newdata, TRUE);
			
			$headers = array(
			'From' => $from,
			'To' =>$to,
			'Subject' => $subject,
			'MIME-Version' => '1.0',
			'Content-Type' => "text/html; charset=ISO-8859-1"
			);

		$smtp = Mail::factory('smtp', array(
			'host' => 'ssl://smtp.gmail.com',
			'port' => '465',
			'auth' => true,
			'username' => 'rupali.jain@lyxellabs.com',
			'password' => 'rupali-28basoya'
			));
		$mail = $smtp->send($to, $headers, $body);
		//print_r($mail); exit;
		if (PEAR::isError($mail)) {
			echo "0"; //mail failed
			} else {
		
			echo "1"; //mail sent
		
			}
			}
			else{
			echo "4"; //something went wrong
			}
			
		}
		else 
		{
		echo "3"; //email id not given
		}
	}

	public function fblogin()
	{    
		$user_fb_id = $this->input->post('fb_id'); 
		$email = $this->input->post('email'); 
		$this->load->model('Data');
		$socialdata = $this->Data->availemail($email);
		if(count($socialdata)>0){
			if($socialdata[0]["social_id"]==''){
			$imgdata = "http://graph.facebook.com/" . $user_fb_id . "/picture?type=normal&height=553&width=553";
			$data = file_get_contents($imgdata);
			$fileName = ".$user_fb_id." . '-' . $user_fb_id . '.jpg';
			$file = fopen('uploads/profilepic/' . $fileName, 'w+');
			fputs($file, $data);
			fclose($file);
			$imgurl = 'uploads/profilepic/' . $fileName;
			$social_id = $this->Data->insert_socialdata($imgurl);  
		$this->Data->update_signupdata($social_id,$socialdata[0]["id"]); 
			}
		$logindetail = $this->Data->logindetail($socialdata[0]["id"]);
		$this->session->set_userdata('registrationid', $logindetail[0]["id"]); 
		echo "1";//user logged in with fb 
		}
		else{
			$imgdata = "http://graph.facebook.com/" . $user_fb_id . "/picture?type=normal&height=553&width=553";
			$data = file_get_contents($imgdata);
			$fileName = ".$user_fb_id." . '-' . $user_fb_id . '.jpg';
			$file = fopen('uploads/profilepic/' . $fileName, 'w+');
			fputs($file, $data);
			fclose($file);
			$imgurl = 'uploads/profilepic/' . $fileName;
			$social_id = $this->Data->insert_socialdata($imgurl);  
			$signupid = $this->Data->insert_signupdata($social_id); 
			$loginid = $this->Data->insert_logindetail($signupid);   
			if($loginid){
			$this->session->set_userdata('registrationid', $loginid);   
			echo "1";//user logged in
			}
			else{
			echo "2";//something wrong happened
			}
		}
		
	}

	public function googlepluslogin()
	{    
		$google_id = $this->input->post('google_id'); 
		$email = $this->input->post('email'); 
		$this->load->model('Data');
		$socialdata = $this->Data->availemail($email);
		if(count($socialdata)>0){
			if($socialdata[0]["social_id"]==''){
			
			$social_id = $this->Data->insert_socialdata_googleplus();  
			$this->Data->update_signupdata($social_id,$socialdata[0]["id"]); 
			}
		$logindetail = $this->Data->logindetail($socialdata[0]["id"]);
		$this->session->set_userdata('registrationid', $logindetail[0]["id"]); 
		echo "1";//user logged in with fb 
		}
		else{
			//echo "string"; exit;        
			$social_id = $this->Data->insert_socialdata_googleplus();  
			$signupid = $this->Data->insert_signupdata($social_id); 
			$loginid = $this->Data->insert_logindetail($signupid);   
			if($loginid){
			$this->session->set_userdata('registrationid', $loginid);   
			echo "1";//user logged in
			}
			else{
			echo "2";//something wrong happened
			}
		}
		
	} 
		public function contactus()
		{
			$this->load->view('include/header_main.php');
			$this->load->view('pages/contactus.php');
		}
		public function submitquery()
		{
			$data = array(
			'name' => $this->security->xss_clean($this->input->post('name')),
			'email' => $this->security->xss_clean($this->input->post('email')),
			'purpose' => $this->security->xss_clean($this->input->post('purpose')),
			'query' => $this->security->xss_clean($this->input->post('userquery')),
			);
			$this->load->model('Data');
			$queryid = $this->Data->insertquery($data);
			if(count($queryid)<=0){
				echo "0";//something went wrong
			}
			else{
				echo "1"; //query has submitted
			}
		}
		public function privacy()
		{
			$this->load->view('include/header_main.php');
			$this->load->view('pages/privacy.php');
		}

		public function edit()
		{
		  
		    $file= APPPATH . 'libraries/dictionaries/custom.txt';
	        $data['text']=file_get_contents($file);
	  
			$this->load->view('pages/edit.php',$data);
		   
		}

		public function saveFile()
		{
			$changedData=$this->input->post('dictionary');
			
		    $file= APPPATH . 'libraries/dictionaries/custom.txt';
			file_put_contents($file,$changedData);
			$data['text']=$changedData;

			
			$data['msg']="Data saved successfully";
			$this->load->view('pages/edit.php',$data);
		   
		}
	}
