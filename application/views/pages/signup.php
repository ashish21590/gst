    <!-- Intro Section -->
    <section id="signup" class="signupbg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 loginsignupinner">
                    
                    <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 signupcontent"> 
                        <h3> Your <span>myGSTrate </span> Account comes with a lot of benefits</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan lorem maximus est vestibulum, pellentesque vulputate nibh tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
                        <h4>Create an Account Today! </h4>
                        <form class="signupform">
                            <span class="errormsg" id="signuperrormsg"></span>
                            <input type="text" id="name" name="name" required onblur="isfilled(this.value)" placeholder="Your Name" class="textbox1">
                            <input type="text" id="email" onkeypress="isvalid(this.value)" name="email" required placeholder="Your Email ID" class="textbox1">
                            <input type="text" maxlength="10" id="phno" onkeypress="isnumber(this.value)" name="phno" required placeholder="Your Contact No." class="textbox1 halftextbox">
                            <div class="passowrdsectn">
                               <input type="password" required minlength="5" id="pwd" name="pwd" placeholder="Your Password" class="textbox1" required autocomplete="off"/>
                               <label class="input-group-addon showhidebtn" id="showhide"><input type="checkbox" style="display:none"onclick="(function(e, el){document.getElementById('pwd').type = el.checked ? 'text' : 'password';el.parentNode.lastElementChild.innerHTML = el.checked ? '<i class=\'fa fa-eye-slash\'>' : '<i class=\'fa fa-eye\'>';})(event, this)"><span><i class="fa fa-eye"></i></span></label>
                            </div>
                            <input type="button" value="Get started" class="getstarted-btn" id="signupbtn"> <span class="smallmsg"> <input type="checkbox" name="" value="" class="check1"> Send me awesome newsletters. </span>
                        </form>
<div class="signupform otpform otp1">
                            <input type="text" name="otp" id="otp" placeholder="Enter OTP sent to your Email Address" class="textbox1">
                            <button name="resendotp" id="resendotp">Resend OTP</button><button name="submitotp" id="submitotp">SUBMIT</button>
                        </div>
                    </div> <!-- /signupcontent-->
                    
                </div> <!--/loginsignupinner -->
            </div>
        </div>
      
    </section>
      
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mycustom.js"></script>

</body>
<script>
function isfilled(value){
if(value == ""){
        $('#signuperrormsg').html("Please enter your full name");
       
    } else {
        $('#signuperrormsg').html("");
    }
}
function isvalid(value) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(value) == false){
     
        $('#signuperrormsg').html("Please enter valid email address");
    } else {
        $('#signuperrormsg').html("");
    }
}

function isnumber(value) {
    var pattern = /^\d{10}$/;
    if(isNaN(value)){
    $('#signuperrormsg').html("Please enter valid contact number");
} 
if (pattern.test(value) == false) {
        $('#signuperrormsg').html("Please enter valid contact number");
       
    }
else {
        $('#signuperrormsg').html("");
    }
}

$( document ).ready(function() {
   
 $(".otp1").hide();
 $("#signupbtn").click(function () {
//alert("hi");
    var formname="signup";
		 var name=$('#name').val();
		 var email=$('#email').val();
		 var phno=$('#phno').val();
       var pwd= $( "#pwd").val(); 
        var pattern = /^\d{10}$/;
 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
 if(name == ""){
        $('#signuperrormsg').html("Please enter your full name");
     
        return false;
    } 
    if(reg.test(email) == false){
        $('#signuperrormsg').html("Please enter valid email address");
     
        return false;
    } 
    if (isNaN(phno)) {
        $('#signuperrormsg').html("Please enter valid contact number");
      
        return false;
    }
    if (pattern.test(phno) == false) {
        $('#signuperrormsg').html("Please enter valid contact number");
        return false;
    }
	if(pwd == '')
	{
		  $('#signuperrormsg').html("Please enter valid password");
     
        return false;
	}
   
		  else {
        var data =
        {
			"formname": formname,
            "name": name,
            "email": email,
            "phno": phno,
            "pwd": pwd,           
        };
		//console.log(data);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>welcome/verify",
            data: data,
            success: function (response) {
                console.log(response);
				var html= response;
				if(html=='1'){
					 $(".otp1").show();
				}
                                 }
            
                        });
        return false;
    }	
    })
	
	 $("#submitotp").click(function () {
		 var otp=$('#otp').val();
		 var name=$('#name').val();
		 var email=$('#email').val();
		  if(name == ""){
        $('#otp').html("Please enter your full name");
     
        return false;
    } 
	 else {
        var data =
        {
			"otp": otp,
            "name": name,
            "email": email,
        };
		//console.log(data);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>welcome/verifyotp",
            data: data,
            success: function (response) {
                console.log(response);
				var html= response;
				if(html=='0'){
					 alert("otp expired");
				}
				else if(html=='2'){
					 alert("User do not exist");
				}
				else{
					 alert("otp verified");
				}
				
                                 }
            
                        });
        return false;
    }	
		 
	 })
	 
	  $("#resendotp").click(function () {
		  $('#otp').val('');
    var formname="signup";
		 var name=$('#name').val();
		 var email=$('#email').val();
        var pattern = /^\d{10}$/;
 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
 if(name == ""){
        $('#error').html("Please enter your full name");
     
        return false;
    } 
    if(reg.test(email) == false){
        $('#error').html("Please enter valid email address");
     
        return false;
    } 
   
    
		  else {
        var data =

        {
			"formname": formname,
            "name": name,
            "email": email,
        };
		//console.log(data);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>welcome/resendotp",
            data: data,
            success: function (response) {
                console.log(response);
				var html= response;
				if(html=='1'){
					alert("OTP sent to your email address");
				}
                                 }
            
                        });
        return false;
    }	
    })

    });
</script>
</html>
