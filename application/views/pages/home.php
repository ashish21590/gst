    <!-- Intro Section -->
    <section id="intro" class="intro-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-10 col-xs-12 intro-inner">
                <form action="getData" onsubmit="return myFunction();" method="GET" role="form">
                    <div class="col-lg-12 col-sm-12 col-xs-12 nopadding">
                    <img src="<?php echo base_url(); ?>assets/images/mainlogo.jpg" class="img-responsive"> 
                    <div class="col-lg-12 nopadding searchfieldform" id="search">
                        <button type="submit" class="btn btn-primary searchbutton"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <input type="text" name="searchQuery" value="" placeholder ="Enter your search..." class="searchbox" id="search-box" autocomplete="off"> 
                    <div id="suggesstion-box"></div> 
</div> </div>
                        </form>

                     
<p>Get GST Rates for your Goods & Services. </p>
                </div>
            </div>
        </div>
        <div class="scrollbtn">Know more about GST <a class="btn btn-default btn-circle page-scroll" href="#about"><i class="fa fa-angle-down"> </i></a></div>
    </section>

    <!-- About Section -->
    <section id="about" class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12 aboutsectn-inner">
                    <h3>What is GST?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper enim a eros tristique feugiat. Aenean a faucibus nisl. Vestibulum ultricies augue maximus, ultricies ipsum vel, vestibulum tortor. Pellentesque vitae felis quis sapien hendrerit egestas et in velit.  </p>
                    <button class="knowmorebtn">Know More</button>
                </div>
            </div>
        </div>
    </section>

    <!-- Services Section -->
    <section id="moreabtgst" class="moreabtgst-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12 moreabtgst-inner">
                    <h3>myGSTrate to the rescue.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper enim a eros tristique feugiat. Aenean a faucibus nisl. Vestibulum ultricies augue maximus, ultricies ipsum vel, vestibulum tortor. Pellentesque vitae felis quis sapien hendrerit egestas et in velit.  </p>
                    <button class="knowmorebtn">Know More</button>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mycustom.js"></script>

</body>
<script>
//for search suggetion
$(document).ready(function(){
    $("#search-box").keyup(function(){
         if($(this).val().length >= 3)
  {
        $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/welcome/searchsuggetionnew",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    }
    else
    {
        $("#suggesstion-box").hide();

    }
    });
});

function selectCountry(val) {
$("#search-box").val(val);
$("#suggesstion-box").hide();
}
//for search suggetion ends here
//for send search query
$(document).on('click', '#searching', function () {
    //alert('hi');
     var searchQuery = $("#search-box").val();
    //alert(searchQuery);
    var data =
        {
            "searchQuery": searchQuery,
            
        };
     $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>index.php/welcome/getData",
            data: data,
            success: function (html) {
                alert(html);
               
            }

        });

});
</script>

</html>
