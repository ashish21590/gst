

    <!-- Intro Section -->
    <section id="contactus" class="contactusbg">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 nopadding contactusbanner">
          <img src="<?php echo base_url();?>assets/images/contactus-banner.jpg" class="img-responsive hidden-md hidden-sm hidden-xs">
          <img src="<?php echo base_url();?>assets/images/contactus-banner.jpg" class="img-responsive hidden-lg hidden-md hidden-xs"> 
          <img src="<?php echo base_url();?>assets/images/contactus-banner.jpg" class="img-responsive hidden-lg hidden-md hidden-sm">  
        </div>
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contactuscon">
                    <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 contactusinner"> 
                        <form class="contactusform" id="clearform">
                            <h3>Get in touch with us. </h3>
                            <span class="errormsg" id="loginerrormsg"> </span>
                            <span class="errormsg" id="loginerrormsg"></span>
                            <input type="text" id="name"  name="name" required placeholder="Your Full Name" class="textbox1" onblur="isfilled(this.value)">
                            <input type="text" name="email" id="email" onkeypress="isvalid(this.value)" placeholder="Your Email ID" class="textbox1">
                            <input type="text" name="purpose" required placeholder="Purpose*" id="purpose" class="textbox1">
           					<textarea placeholder="Query" name="userquery" id="userquery" class="textbox1"></textarea>
                            <input type="button" value="submit" id="submitbtn" class="submit-btn">
                        </form>
                        <form class="contactusform" style="display:none;">
                            <h3 id="thankyoumodal"></h3>
                        </form>
                    </div> <!-- /contactusinner-->
                    
                </div> <!--/contactuscon -->
            </div>
        </div>
      
    </section>
    <footer class="footer">
      <div class="container">
        <span><i>&copy; myGSTrate.</i> Get GST Rates for your Goods and Services.</span>
      </div>
    </footer>

   
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mycustom.js"></script>

</body>
<script>
function isfilled(value){
if(value == ""){
        $('#loginerrormsg').html("Please enter your full name");
       
    } else {
        $('#loginerrormsg').html("");
    }
}
function isvalid(value) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(value) == false){
     
        $('#loginerrormsg').html("Please enter valid email address");
    } else {
        $('#loginerrormsg').html("");
    }
}

$( document ).ready(function() {
   
 $("#submitbtn").click(function () {
	  		var name=$('#name').val();
		 	var email=$('#email').val();
        	var purpose= $('#purpose').val();
        	var userquery = $('#userquery').val();
 			var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     
	 if(name == ""){
        $('#loginerrormsg').html("Please enter your full name");
     
        return false;
    } 
    if(reg.test(email) == false){
        $('#loginerrormsg').html("Please enter valid email address");
     
        return false;
    } 
 
    if(purpose == ""){
        $('#loginerrormsg').html("Please enter the purpose of query");
        return false;
    } 
   
		  else {
        var data =
        {
            "name": name,
            "email": email,
            "userquery": userquery,
            "purpose": purpose,
        };
		//console.log(data);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>Welcome/submitquery",
            data: data,
             success: function (html) {
                
                 console.log(html);
                 if(html==1)
                 {
                   //console.log(content.status);
				$('.contactusform').show();
				  $('#thankyoumodal').html("Thankyou for Contacting us.");
                    $('#clearform')[0].reset();
                }
             
             else{
              
                  $('.contactusform').show();
				  $('#thankyoumodal').html("Something went wrong");
                    $('#clearform')[0].reset();
                }
                                 }
            
                        });
        return false;
    }	
    })
	
	

    });
</script>

</html>
