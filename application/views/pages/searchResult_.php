<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

        
<div class="container">
  <h2>Search result for : <?php print_r($queryParameter); ?></h2>
  <p> 
  <div class="alert alert-success">
    About <?php print_r($totalResultRows); ?> results.
  </div>
  </p>            
  <table class="table">
    <thead>
      <tr>
        <th>Main_Category</th>  
        <th>Second_Category</th> 
        <th>Third_Category</th>   
        <th>HSN Code</th>
        <th>Unique Reference</th>
        <th>Description of Goods</th>
        <th>IGST Rate</th>
        <th>Chapter No </th>
        <th>Subject_Code </th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($allData as $value) {
        # code...
     ?>
      <tr>
        <td><?php echo $value['Main_Category']; ?></td>
        <td><?php echo $value['Second_Category']; ?></td>
        <td><?php echo $value['Third_Category']; ?></td>
        <td><?php echo $value['HSN_Code']; ?></td>
        <td><?php echo $value['Unique_Reference']; ?></td>
        <td><?php echo $value['Description_of_goods']; ?></td>
        <td><?php echo $value['IGST_Rate']; ?></td>
        <td><?php echo $value['Chapter_Name']; ?></td>
        <td><?php echo $value['Subject_Code']; ?></td>
        
      </tr>

      <?php } ?>
      
    </tbody>
  </table>
   <div class="mypaginate">
    <p><?php echo $pagination; ?></p>
   </div>
</div>
       <style>
        .mypaginate{
            margin: 0 auto;
           

       }
       </style>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>