    <!-- Intro Section -->
    <section id="signup" class="signupbg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 loginsignupinner">
                    <div class=" col-lg-5 col-md-6 col-sm-12 col-xs-12 logincontent"> 
                    
                        <form class="loginform"> 
                            <div class="col-lg-12 col-md-12 nopadding"> <img src="<?php echo base_url(); ?>assets/images/small-logo.jpg" class="img-responsive"> </div>
                            <label>Log in to your <span> myGSTrate </span> Account </label>
                            <span class="errormsg" id="loginerrormsg"></span>
                            <input type="text" name="email" id="email" onkeypress="isvalid(this.value)" placeholder="Your Email ID" class="textbox1">
                            <div class="passowrdsectn">
                               <input type="password" required minlength="5" id="pwd" name="pwd" placeholder="Password" class="textbox1" required autocomplete="off"/>
                                <label class="input-group-addon showhidebtn" id="showhide"><input type="checkbox" style="display:none"onclick="(function(e, el){document.getElementById('pwd').type = el.checked ? 'text' : 'password';el.parentNode.lastElementChild.innerHTML = el.checked ? '<i class=\'fa fa-eye-slash\'>' : '<i class=\'fa fa-eye\'>';})(event, this)"><span><i class="fa fa-eye"></i></span></label>
                            </div>
                            <div> <input type="button" value="Log in" class="login-btn" id="loginbtn"> <span class="smallmsg"> Problem Logging in? </span>
                            </div>                       
                          
                            
                        </form>
                          <div class="divider"> <hr> <span>OR </span> </div>
                          <div class="col-lg-12 col-sm-12 viafbgplusctn nopadding">
                              <button onclick="myFacebookLogin()" class="signupfb-btn">sign up with facebook</button>
                               
                              <button class="signupgplus-btn" id="signin-button">sign up with google</button>

                            </div>
                    </div> <!-- /logincontent-->
                    
                </div> <!--/loginsignupinner -->
            </div>
        </div>
      
    </section>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mycustom.js"></script>
   
    


</body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
    appId      : '1266334523475343', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.10' // use graph api version 2.8
    });
 
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<script>

// Only works after `FB.init` is called
function myFacebookLogin() {

 FB.login(function(response) {
    if (response.authResponse) {
     console.log('Welcome!  Fetching your information.... ');
      FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture,birthday'},
    function (response) {
      console.log(response);
    var data = {
              "fb_id": response.id,
              "first_name": response.first_name,
              "last_name": response.last_name,
              "email": response.email,
              "link": response.link,
              "gender": response.gender,
              "locale": response.locale,
              "picture": response['picture']['data']['url'],
              "birthday": response.birthday,
              
            };
      $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>Welcome/fblogin",
              data: data,
              success: function (html) {
                //alert(html);
                //console.log(response);
                if (html == 1) {
                 // alert("you are logged in");
                  window.location.href = "<?php echo base_url();?>" ;
                  } 
                  else{
$('#loginerrormsg').html("user not registered");
         
        }
              }
            });  //ajax ends here
    });
         
    } else {
     console.log('User cancelled login or did not fully authorize.');
    }
}, {scope: 'email'});
}


</script>
<script>
function isvalid(value) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(value) == false){
     
        $('#loginerrormsg').html("Please enter valid email address");
    } else {
        $('#loginerrormsg').html("");
    }
}

$( document ).ready(function() {
   
 $("#loginbtn").click(function () {
     var email=$('#email').val();
        var pwd= $( "#pwd").val(); 
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     
    if(reg.test(email) == false){
        $('#loginerrormsg').html("Please enter valid email address");
     
        return false;
    } 
  if(pwd == '')
  {
      $('#loginerrormsg').html("Please enter valid password");
     
        return false;
  }
      else {
        var data =
        {
            "email": email,
            "pwd": pwd,           
        };
    //console.log(data);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>welcome/logindetail",
            data: data,
            success: function (response) {
                console.log(response);
        var html= response;
        if(html=='1'){
         
          window.location.href = "<?php echo base_url();?>" ;
        }
        else{
          $('#loginerrormsg').html("user not registered. Please first register to use features");
          
          
        }
                                 }
            
                        });
        return false;
    } 
    })
  
  

    });
</script>
<!-- for google plus sign up script starts here -->
<script type="text/javascript">
var auth2 = {};
var helper = (function() {
  return {
    /**
     * Hides the sign in button and starts the post-authorization operations.
     *
     * @param {Object} authResult An Object which contains the access token and
     *   other authentication information.
     */
    onSignInCallback: function(authResult) {
    
      if (authResult.isSignedIn.get()) {
       // $('#authOps').show('slow');
       // $('#gConnect').hide();
        helper.profile();
        helper.people();
      } else {
          if (authResult['error'] || authResult.currentUser.get().getAuthResponse() == null) {
            // There was an error, which means the user is not signed in.
            // As an example, you can handle by writing to the console:
            console.log('There was an error: ' + authResult['error']);
          }
         // $('#authResult').append('Logged out');
         // $('#authOps').hide('slow');
         // $('#gConnect').show();
      }
      console.log('authResult', authResult);
    },
    /**
     * Calls the OAuth2 endpoint to disconnect the app for the user.
     */
    disconnect: function() {
      // Revoke the access token.
      auth2.disconnect();
    },
    /**
     * Gets and renders the list of people visible to this app.
     */
    people: function() {
      gapi.client.plus.people.list({
        'userId': 'me',
        'collection': 'visible'
      }).then(function(res) {
        var people = res.result;
       
      });
    },
    /**
     * Gets and renders the currently signed in user's profile data.
     */
    profile: function(){
      gapi.client.plus.people.get({
        'userId': 'me'
      }).then(function(res) {
        var profile = res.result;
     //console.log(profile);
        var data = {
              "google_id": profile.id,
              "first_name": profile.name.givenName,
              "last_name": profile.name.familyName,
              "email": profile.emails[0].value,
              "google_link": profile.url,
              "gender": profile.gender, 
               "display_name": profile.displayName,
              "language": profile.language,
              "picture": profile.image.url,
             // "cover_picture": profile.cover.coverPhoto.url,
            };
            console.log(data);
            // ajax start here 
      $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>Welcome/googlepluslogin",
              data: data,
              success: function (html) {
                //alert(html);
                //console.log(response);
                if(html=='1'){
         
          window.location.href = "<?php echo base_url();?>" ;
        }
        else{
          $('#loginerrormsg').html("user not registered. Please first register to use features");
          
          
        }
              }
            });  //ajax ends here
       
      }, function(err) {
        var error = err.result;
        $('#profile').empty();
        $('#profile').append(error.message);
      });
    }
  };
})();
/**
 * jQuery initialization
 */
$(document).ready(function() {
  $('#disconnect').click(helper.disconnect);
  
});
/**
 * Handler for when the sign-in state changes.
 *
 * @param {boolean} isSignedIn The new signed in state.
 */
var updateSignIn = function() {
  console.log('update sign in state');
  if (auth2.isSignedIn.get()) {
    console.log('signed in');
    helper.onSignInCallback(gapi.auth2.getAuthInstance());
  }else{
    console.log('signed out');
    helper.onSignInCallback(gapi.auth2.getAuthInstance());
  }
}
/**
 * This method sets up the sign-in listener after the client library loads.
 */
function startApp() {
  gapi.load('auth2', function() {
    gapi.client.load('plus','v1').then(function() {
      gapi.signin2.render('signin-button', {
          scope: 'https://www.googleapis.com/auth/plus.login',
          fetch_basic_profile: false });
      gapi.auth2.init({fetch_basic_profile: false,
          scope:'https://www.googleapis.com/auth/plus.login'}).then(
            function (){
              console.log('init');
              auth2 = gapi.auth2.getAuthInstance();
              auth2.isSignedIn.listen(updateSignIn);
              auth2.then(updateSignIn);
            });
    });
  });
}
//the user is signed out when they leave the page
window.onbeforeunload = function(e){
  gapi.auth2.getAuthInstance().signOut();
};
</script>
<script src="https://apis.google.com/js/client:platform.js?onload=startApp"></script>
<!-- for google plus sign up script ends here -->

</html>
