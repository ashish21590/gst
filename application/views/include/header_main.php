<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
     <meta name="google-signin-client_id" content="217831719167-8no217p83jeiv1vllsbo28crfn8b7i1e.apps.googleusercontent.com"></meta>

    <title>Main Page</title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body>
 
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="hidden">
                        <a class="page-scroll" href="#page-top"></a>
                    </li>
                    <li>
                        <a><i class="fa fa-quora" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li><button type="button" class="navbar-toggle" data-toggle="collapse" onclick="openNav()" style="display:block;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

            <div id="mySidenav" class="sidenav">
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
              <ul>
                <li> <label>MENU </label> </li>
                <li><a href="#">About </a> </li>
                <li><a href="<?php echo base_url();?>privacy">privacy">Privacy </a> </li>
                <li><a href="#">Terms of Use </a> </li>
                <li><a href="<?php echo base_url();?>contactus">Contact </a> </li>
                <li><a href="#">Help </a> </li>
                <li><a href="#">Careers </a> </li>
              </ul>
              <ul>
                <li> <label>KEEP IN TOUCH </label> </li>
                <li><a href="#">Facebook </a> </li>
                <li><a href="#">Twitter </a> </li>
                <li><a href="#">Quora </a> </li>
                <li><a href="#">Blog </a> </li>
              </ul>
               <a href="<?php echo base_url();?>signup"><button class="signup-btn">sign up</button></a>
             <a href="<?php echo base_url();?>login"> <button class="login-btn">Log in</button></a>
              <p>2017 Copyright. All rights reserved. </p>
            </div>
    </nav>