      <?php

      class Data extends CI_Model
      {

        /**
        * Responsable for auto load the database
        * @return void
        */
        public function __construct()
        {
          $this->load->database();
        }

        /**
        * Get product by his is
        * @param int $product_id
        * @return array
        */
        public function getData($queryString,$limit, $start,$orderBy)
        {   
          $res=[];
          
          $cleanQuery= addslashes($this->db->escape_like_str($queryString)); 
          //ALTER TABLE mytable ADD FULLTEXT(`Main_Category`,`Second_Category`,`Third_Category`,`Searchable_Query`)
         /*  $query="SELECT * FROM mytable WHERE MATCH(`Main_Category`,`Second_Category`,
                  `Third_Category`,`Searchable_Query`) AGAINST('.$cleanQuery.')
                  order by `Sr_No` $orderBy  limit $limit, $start"; exit; */
           $query="SELECT * FROM mytable WHERE MATCH(`Main_Category`,`Second_Category`,
                  `Third_Category`,`Searchable_Query`) AGAINST('.$cleanQuery.')
                     limit $limit, $start";
        // $res['totalRows']=$this->db->query($query)->num_rows();
          //print_r($res); exit;
          $res['results'] = $this->db->query($query)->result_array();
          return $res;

        } // for the text search part

        public function allData($queryString)
        {   
          $res=[];
          $queryString= htmlentities($queryString);
          $cleanQuery=addslashes(urldecode($queryString));
        $query="SELECT * FROM mytable WHERE MATCH(`Main_Category`,`Second_Category`,
                  `Third_Category`,`Searchable_Query`) AGAINST('.$cleanQuery.')";
                  $res['totalRows']=$this->db->query($query)->num_rows();
          //print_r($res); exit;
        // $res['results'] = $this->db->query($query)->result_array();
          return $res;

        }


        public function userAgent($userAgent,$queryParameter)
        {
            
            $data['ip_address'] ='';
            $data['browser'] =$userAgent->browser;
            $data['mobile'] =$userAgent->is_mobile;
            $data['platform'] =$userAgent->platform;
            $data['browser_version'] =$userAgent->version;
            $data['robot'] =$userAgent->robot;
            $data['query']=$queryParameter;
            $this->db->insert('user_agent',$data);
            return $this->db->insert_id();
        }

        

      public function searchsuggetion_second($keyword){ 

          $this->db->select('Second_Category AS text')->from('mytable'); 
          $this->db->like('Second_Category',$keyword, 'after');
          $this->db->group_by('Second_Category'); 
        // $this->db->or_like('iso',$keyword,'after'); 
          $this->db->limit(5);
          $query = $this->db->get(); 
          return $query->result_array(); 
      }

      public function searchsuggetion_third($keyword){ 

          $this->db->select('Third_Category AS text')->from('mytable'); 
          $this->db->like('Third_Category', $keyword, 'after');
          $this->db->group_by('Third_Category'); 
        // $this->db->or_like('iso',$keyword,'after'); 
          $this->db->limit(5);
          $query = $this->db->get(); 
          return $query->result_array(); 
      }

  public function getuserdata($email,$pwd)
  {
      $this->db->select('*');
      $this->db->from('login');
      $this->db->where('email', $email);
      $this->db->where('password', $pwd);
      $query = $this->db->get();
      return $query->result_array();

  }
  public function availemail($email)
  {
      $this->db->select('*');
      $this->db->from('signup');
      $this->db->where('email', $email);
      $query = $this->db->get();
      return $query->result_array();
  }
  public function insert_random($ran_var)
  { 
          $data = array(
          'Name' => $this->input->post('name'),
          'email' => $this->input->post('email'),
          'phno' => $this->input->post('phno'),
          'password' => $this->input->post('pwd'),
          'rand_no' => $ran_var,
          'formname' => $this->input->post('formname'),
          );


      $this->db->insert('signup', $data);
      return $this->db->insert_id();
  }
  public function update_random($ran_var, $id)
  { 
          $data = array(
          'phno' => $this->input->post('phno'),
          'password' => $this->input->post('pwd'),
          'rand_no' => $ran_var,
          'formname' => $this->input->post('formname'),
          );
      $this->db->where('id', $id);
      $this->db->update('signup', $data);
  }
      public function logindata($id)
  { 
          $data = array(
          'signupid' => $id,
          'email' => $this->input->post('email'),
          'password' => $this->input->post('pwd'),
          );


      return $this->db->insert('login', $data);
  }
  public function updatelogindetail($signupid)
  { 
          $data = array(
          'password' => $this->input->post('pwd'),
          );
      $this->db->where('signupid', $signupid);
      $this->db->update('login', $data);
  }
      public function verifyuser()
  { 
          $otp=$this->input->post('otp');
          $email=$this->input->post('email');
          $this->db->select('*');
              $this->db->from('signup');
              $this->db->where('email', $email);
              $this->db->where('rand_no', $otp);

              $query = $this->db->get();
              
              return $query->result_array();
  }
  public function updatestatus($id)

  {

      $data = array('status' => 1);
      $this->db->where('id', $id);
    return $this->db->update('signup', $data);
          //return $result;
          //print_r($data);
  }
  public function insert_otp($ran_var)

  {
      $email = $this->input->post('email');
      $data = array('rand_no' => $ran_var);
      $this->db->where('email', $email);
    return $this->db->update('signup', $data);
          //return $result;
          //print_r($data);
  }
      public function logindetail($signupid)
  { 
          $this->db->select('*');
              $this->db->from('login');
              $this->db->where('signupid', $signupid);
              $query = $this->db->get();          
              return $query->result_array();
  }
      public function insert_socialdata($imgurl)
  { 
          $data = array(
        'facebookid' => $this->input->post('fb_id'),
          'first_name' => $this->input->post('first_name'),
          'last_name' => $this->input->post('last_name'),
          'email' => $this->input->post('email'),
          'link' => $this->input->post('link'),
          'gender' => $this->input->post('gender'),
          'locale' => $this->input->post('locale'),
          'picture' => $imgurl,
          'birthday' => $this->input->post('birthday'),
        
          );
      $this->db->insert('social_login', $data);
      return $this->db->insert_id();
  }
  public function update_signupdata($socialid,$signupid)

  {
      $data = array('social_id' => $socialid);
      $this->db->where('id', $signupid);
    return $this->db->update('signup', $data);
          //return $result;
          //print_r($data);
  }
      public function insert_signupdata($socialid)
  {  
          $data = array(
          'Name' => $this->input->post('first_name')." ".$this->input->post('last_name'),
          'email' => $this->input->post('email'),
          'social_id' => $socialid,
          'status' => 1,
          );


      $this->db->insert('signup', $data);
      return $this->db->insert_id();
  }
  public function insert_logindetail($signupid)
  {  
          $data = array(
          'signupid' => $signupid,
          'email' => $this->input->post('email'),
          );


      $this->db->insert('login', $data);
      return $this->db->insert_id();
  }

  public function numberSearch($queryString,$limit, $start)
          {
            
          
            
            $res=[];;
            $arr=[];
            $paramersLike="";
            
        if(is_array($queryString)){
          $sr=1;
          foreach($queryString as $queries){
            
            if(count($queryString)==$sr){
            $paramersLike.="`HSN_Code` like "."'".$queries."%'";
            }
            else{
              $paramersLike.="`HSN_Code` like "."'".$queries."%'"." OR ";
            }
            $sr++;
          }

        }
        else{
        
          $arr[0]=$queryString;
          $sr=1;
          foreach($arr as $queries){
          
            
            if(count($arr)==$sr){
            $paramersLike.="`HSN_Code` like "."'".$queries."%'";
            }
            else{
              $paramersLike.="`HSN_Code` like "."'".$queries."%'"." OR ";
            }
            $sr++;
          }

        }    
        
        // $queryString= htmlentities($queryString);                
            
            $query="SELECT * FROM mytable WHERE $paramersLike limit $limit, $start ";
          
            $res['results'] = $this->db->query($query)->result_array();           
            return $res;

        } 

        public function numberSearcAll($queryString)
          {   
                $res=[];;
                $arr=[];
              
                $paramersLike="";
                
            if(is_array($queryString)){
              $sr=1;
              foreach($queryString as $queries){
                
                if(count($queryString)==$sr){
                $paramersLike.="`HSN_Code` like "."'".$queries."%'";
                }
                else{
                  $paramersLike.="`HSN_Code` like "."'".$queries."%'"." OR ";
                }
                $sr++;
              }
        
            }
            else{
            
              $arr[0]=$queryString;
              $sr=1;
              foreach($arr as $queries){
              
                
                if(count($arr)==$sr){
                $paramersLike.="`HSN_Code` like "."'".$queries."%'";
                }
                else{
                  $paramersLike.="`HSN_Code` like "."'".$queries."%'"." OR ";
                }
                $sr++;
              }
        
            }
            
          
          
            
            // $queryString= htmlentities($queryString);                
                $query=$query="SELECT * FROM mytable WHERE $paramersLike";
                $res['totalRows']=$this->db->query($query)->num_rows();
                $res['results'] = $this->db->query($query)->result_array();           
                return $res;
        
          }

          //for google plus insert in social tabel
        public function insert_socialdata_googleplus()
          { 
          $data = array(
        'google_id' => $this->input->post('google_id'),
          'first_name' => $this->input->post('first_name'),
          'last_name' => $this->input->post('last_name'),
          'email' => $this->input->post('email'),
          'google_link' => $this->input->post('google_link'),
          'gender' => $this->input->post('gender'),
          'display_name' => $this->input->post('display_name'),
          'picture' => $this->input->post('picture'), 
          'language' => $this->input->post('language'),
        
          );
      $this->db->insert('social_login', $data);
      return $this->db->insert_id();
        }

    public function insertquery($data)
      {  
          $this->db->insert('contactus', $data);
          return $this->db->insert_id();
      }

      public function percentageSearch($number,$queryParameter=null,$limit, $start)
      {
        $paramersLike='';
        $queryParameter=trim($queryParameter);
        $paramersLike.=" `Searchable_Query` like "."'%".$queryParameter."%' and   `IGST_Rate` like "."'".$number."%'  limit $limit, $start";
        // $queryString= htmlentities($queryString);                
        $query=$query="SELECT * FROM mytable WHERE $paramersLike";
        $res['totalRows']=$this->db->query($query)->num_rows();
        $res['results'] = $this->db->query($query)->result_array();           
        return $res;
      }

      public function percentageSearchTotal($number,$queryParameter=null){
        $paramersLike='';
        $queryParameter=trim($queryParameter);
        $paramersLike.=" `Searchable_Query` like "."'%".$queryParameter."%' and   `IGST_Rate` like "."'".$number."%'";
        // $queryString= htmlentities($queryString);                
        $query=$query="SELECT * FROM mytable WHERE $paramersLike";
        $res['totalRows']=$this->db->query($query)->num_rows();        
        return $res;
      }

  }
